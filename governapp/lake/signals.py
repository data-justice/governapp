from django.db.models.signals import pre_save, post_save, post_delete
from django.dispatch import receiver
from lake import models
#from django.db.models.signals import m2m_changed


@receiver(post_save, sender=models.Island)
def steward_is_member(sender, **kwargs):
    island = kwargs['instance']
    if kwargs['created']:
        island.membership_set.create(member=island.custodian, status='steward')




@receiver(post_save, sender=models.Membership)
def member_has_standing(sender, **kwargs):
    membership = kwargs['instance']
    member = membership.member
    island = membership.island
    if kwargs['created']:
        for issue in island.issue_set.all():
            issue.standing_set.create(member=member,
                                      issue=issue,
                                      tokens=issue.max_tokens)




@receiver(post_delete, sender=models.Membership)
def lose_standing(sender, **kwargs):
    membership = kwargs['instance']
    member = membership.member
    island = membership.island
    for issue in island.issue_set.all():
        for s in issue.standing_set.filter(member=member):
            s.delete()
