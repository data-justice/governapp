from django.apps import AppConfig


class LakeConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'lake'
    verbose_name = 'Lake Democracy'
    
    def ready(self):
        import lake.signals
