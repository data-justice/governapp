from django import forms
from dal import autocomplete
from lake import models

class VoteForm(forms.Form):

    CHOICES=[('-2','-2'),
             ('-1','-1'),
             ('0','0'),
             ('1','1'),
             ('2','2'),
             ]

    weight = forms.ChoiceField(choices=CHOICES,
                               widget=forms.RadioSelect)

    anonymous = forms.BooleanField(required=False)


class MemberForm(forms.ModelForm):
    member = forms.ModelChoiceField(
        queryset=models.User.objects.all(),
        widget=autocomplete.ModelSelect2(url='member-autocomplete')
    )

    class Meta:
        model = models.Membership
        fields = ['member']
