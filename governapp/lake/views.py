from django.shortcuts import render
from django.views.generic import ListView, DetailView
from django.views.generic.base import ContextMixin
from django.views import View
from lake import models
from lake.forms import VoteForm, MemberForm
from django.db import IntegrityError
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from dal import autocomplete
from django.utils.translation import gettext as _


###############
# Island CRUD #
###############

class IslandMixin(ContextMixin):

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        island = models.Island.objects.get(pk=self.kwargs.get('island',
                                                              self.kwargs.get('pk')))
        context['island'] = island

        context['owner'] = True if self.request.user == island.custodian else False

        context['steward'] = True if self.request.user in island.stewards() else False

        if (island.default_government == 'democratic'
            or (island.default_government == 'centralized'
                and context['owner'])):
            context['issue_adder'] = True
        else:
            context['issue_adder'] = False

        return context


class IslandListView(LoginRequiredMixin, ListView):
    model = models.Island

    def get_queryset(self):
        return self.model.objects.filter(membership__member=self.request.user)


class IslandCreateView(LoginRequiredMixin, CreateView):
    model = models.Island
    fields = ['name', 'description', 'default_government', 'avatar']
    template_name = 'lake/island_create.html'

    def form_valid(self, form):
        form.instance.custodian = self.request.user
        return super().form_valid(form)


class IslandUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView, IslandMixin):

    def test_func(self):
        island = models.Island.objects.get(pk=self.kwargs['pk'])
        if self.request.user == island.custodian:
            return True
        else:
            return False


    model = models.Island
    fields = ['name', 'description', 'default_government', 'avatar']


class IslandDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView, IslandMixin):

    def test_func(self):
        island = models.Island.objects.get(pk=self.kwargs['pk'])
        if self.request.user == island.custodian:
            return True
        else:
            return False


    model = models.Island
    success_url = reverse_lazy('island-list')


class IslandDetailView(LoginRequiredMixin, UserPassesTestMixin, DetailView, IslandMixin):

    def test_func(self):
        island = models.Island.objects.get(pk=self.kwargs['pk'])
        print(self.request.user in island.members())
        if self.request.user in island.members():
            return True
        else:
            return False


    model = models.Island


class IslandCommentView(DetailView, IslandMixin):
    model = models.Island
    template_name = 'lake/island_comment.html'


##############
# Issue CRUD #
##############

class IssueMixin(ContextMixin):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        island = models.Island.objects.get(pk=self.kwargs['island'])
        context['island'] = island
        context['owner'] = True if self.request.user == context['island'].custodian else False
        context['issue'] = models.Issue.objects.get(
            pk=self.kwargs.get('issue', self.kwargs.get('pk')))

        if (island.default_government == 'democratic'
            or (island.default_government == 'centralized'
                and context['owner'])):
            context['issue_adder'] = True
        else:
            context['issue_adder'] = False

        return context


class IssueDetail(DetailView, IssueMixin):
    model = models.Issue


class IssueCreateView(LoginRequiredMixin, CreateView, IslandMixin):
    model = models.Issue
    fields = ['title', 'description', 'government', 'max_tokens']

    def get_initial(self):
        island = models.Island.objects.get(pk=self.kwargs['island'])
        return {'government': island.default_government}


    def form_valid(self, form):
        form.instance.custodian = self.request.user
        island = models.Island.objects.get(pk=self.kwargs['island'])
        form.instance.island = island
        form.save()

        member_num = island.membership_set.count()
        if member_num > 0:
            tokens_per_member = float(form.instance.max_tokens) / member_num
            for membership in island.membership_set.all():
                standing = models.Standing(member=membership.member,
                                           issue=form.instance,
                                           tokens=tokens_per_member)
                standing.save()

        return super().form_valid(form)




class IssueUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView, IssueMixin):

    def test_func(self):
        issue = models.Issue.objects.get(pk=self.kwargs['pk'])
        if self.request.user == issue.custodian:
            return True
        else:
            return False

    model = models.Issue
    fields = ['title', 'description', 'government', 'max_tokens']


class IssueDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView, IssueMixin):

    def test_func(self):
        issue = models.Issue.objects.get(pk=self.kwargs['pk'])
        if self.request.user == issue.custodian:
            return True
        else:
            return False

    model = models.Issue

    def get_success_url(self):
        island = self.object.island
        return reverse_lazy( 'island-detail', kwargs={'pk': island.id})


class IssueCommentView(DetailView, IslandMixin):
    model = models.Issue
    template_name = 'lake/issue_comment.html'



class StandingListView(LoginRequiredMixin, ListView, IssueMixin):
    model = models.Standing

    def get_queryset(self):
        issue = models.Issue.objects.get(pk=self.kwargs['issue'])
        return issue.standing_set.all()


class StandingDecIncView(LoginRequiredMixin, UserPassesTestMixin, View):

    def test_func(self):
        island = models.Island.objects.get(pk=self.kwargs['island'])
        issue = models.Issue.objects.get(pk=self.kwargs['issue'])
        alter = models.User.objects.get(username=self.kwargs['username'])
        
        if alter == self.request.user:
            return False
        
        if self.request.user in island.members():
            return True
        else:
            return False

    def get(self, request, *args, **kwargs):
        island = models.Island.objects.get(pk=self.kwargs['island'])
        issue = models.Issue.objects.get(pk=self.kwargs['issue'])
        alter = models.User.objects.get(username=self.kwargs['username'])

        alter_standing = models.Standing.objects.get(member=alter,
                                                     issue=issue)

        ego_standing = models.Standing.objects.get(member=request.user,
                                                   issue=issue)

        delta = self.kwargs['delta']
        if delta == 'decrease':
            if ego_standing.tokens < issue.max_tokens:
                alter_standing.tokens -= 1
                ego_standing.tokens += 1
        elif delta == 'increase':
            if ego_standing.tokens > 0:            
                alter_standing.tokens += 1
                ego_standing.tokens -= 1

        alter_standing.save()
        ego_standing.save()

        return HttpResponseRedirect(reverse('standing-list', args=[island.id,
                                                                   issue.id, ]))



####################
# Alternative CRUD #
####################

class AlternativeDetail(DetailView, IssueMixin):
    model = models.Alternative

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        alternative = context['object']

        context['may_vote'] = True if models.Vote.objects.filter(voter=self.request.user,
                                                                 alternative=alternative).count() == 0 else False

        return context


class AlternativeCreateView(LoginRequiredMixin, CreateView, IssueMixin):
    model = models.Alternative
    fields = ['title', 'description']

    def form_valid(self, form):
        form.instance.author = self.request.user
        form.instance.island = models.Island.objects.get(pk=self.kwargs['island'])
        form.instance.issue = models.Issue.objects.get(pk=self.kwargs['issue'])
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy( 'alternative-detail',
                             kwargs={'island': self.object.island.id,
                                     'issue': self.object.issue.id,
                                     'pk': self.object.id})



class AlternativeUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView, IssueMixin):

    def test_func(self):
        alternative = models.Alternative.objects.get(pk=self.kwargs['pk'])
        if self.request.user == alternative.author:
            return True
        else:
            return False

    def get_success_url(self):
        return reverse_lazy( 'alternative-detail',
                             kwargs={'island': self.object.issue.island.id,
                                     'issue': self.object.issue.id,
                                     'pk': self.object.id})


    model = models.Alternative
    fields = ['title', 'description']


class AlternativeDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView, IssueMixin):

    def test_func(self):
        alternative = models.Alternative.objects.get(pk=self.kwargs['pk'])
        if self.request.user == alternative.author:
            return True
        else:
            return False

    model = models.Alternative

    def get_success_url(self):
        return reverse_lazy( 'alternative-detail', kwargs={'pk': alternative.id,
                                                           'island': self.object.island,
                                                           'issue': self.object.issue})


class AlternativeCommentView(DetailView, IssueMixin):
    model = models.Alternative
    template_name = 'lake/alternative_comment.html'



########
# Vote #
########




class Vote(View):
    form_class = VoteForm
    template_name = 'vote.html'

    def get(self, request, *args, **kwargs):
        alternative = models.Alternative.objects.get(pk=kwargs['alt_id'])
        form = self.form_class()
        my_standing = alternative.issue.standing_set.filter(member=request.user)[0]

        return render(request,
                      self.template_name,
                      {'form': form,
                       'alternative': alternative,
                       'standing': my_standing})

    def post(self, request, *args, **kwargs):
        alternative = models.Alternative.objects.get(pk=kwargs['alt_id'])
        form = self.form_class(request.POST)
        if form.is_valid():
            vote, created = models.Vote.objects.get_or_create(alternative=alternative,
                                                              voter=request.user)
            weight = form.cleaned_data['weight']
            # anonymous = form.cleaned_data['anonymous']
            vote.magnitude = weight
            vote.save()
            return HttpResponseRedirect(reverse('issue-detail', args=[alternative.issue.island.id, alternative.issue.id]))

        return HttpResponseRedirect(reverse('issue-detail', args=[alternative.issue.island.id, alternative.issue.id]))



# class DistributeTokens.as_view()

# class Vote.as_view()



##############
# Membership CRUD #
##############


class MemberAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return models.User.objects.none()

        qs = models.User.objects.all()

        if self.q:
            qs = qs.filter(username__istartswith=self.q)

        return qs



class MembershipCreateView(LoginRequiredMixin, CreateView, IslandMixin):
    model = models.Membership
    form_class = MemberForm

    def form_valid(self, form):
        island = models.Island.objects.get(pk=self.kwargs['island'])
        form.instance.island = island
        form.instance.status = 'member'
        try:
            return super().form_valid(form)
        except IntegrityError:
            form.add_error('member', _('That person is already a member or has already been invited.'))
            return self.form_invalid(form)

    def get_success_url(self):
        island = self.object.island
        return reverse_lazy( 'membership', kwargs={'island': island.id})



class MembershipDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView, IslandMixin):

    def test_func(self):
        island = models.Island.objects.get(pk=self.kwargs['island'])

        if self.request.user == island.custodian:
            return True
        elif self.request.user in island.stewards():
            return True
        else:
            return False

    model = models.Membership

    def get_success_url(self):
        island = self.object.island
        return reverse_lazy( 'membership', kwargs={'island': island.id})




class MembershipStewardStatusView(LoginRequiredMixin, UserPassesTestMixin, View):

    def test_func(self):
        island = models.Island.objects.get(pk=self.kwargs['island'])

        if self.request.user == island.custodian:
            return True
        elif self.request.user in island.stewards():
            return True
        else:
            return False

    def get(self, request, *args, **kwargs):
        island = models.Island.objects.get(pk=self.kwargs['island'])
        m = models.Membership.objects.get(pk=self.kwargs['pk'])
        m.status = 'steward'
        m.save()
        return HttpResponseRedirect(reverse('membership', args=[island.id, ]))


class MembershipListView(LoginRequiredMixin, ListView, IslandMixin):
    model = models.Membership

    def get_queryset(self):
        i = models.Island.objects.get(pk=self.kwargs['island'])
        return i.membership_set.all()


###############
# Profile CRUD
###############


class ProfileCreateView(LoginRequiredMixin, CreateView):
    model = models.Profile
    fields = ['website', 'bio']

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class ProfileUpdateView(LoginRequiredMixin, UpdateView):

    model = models.Profile
    fields = ['website', 'bio']

    def get_object(self):
        profile, created = models.Profile.objects.get_or_create(
            user=models.User.objects.get(username=self.kwargs['username']))

        return profile

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy( 'profile-detail',
                             kwargs={'username': self.request.user})



class ProfileDetailView(DetailView):
    model = models.Profile

    def get_object(self):
        profile, created = models.Profile.objects.get_or_create(
            user=models.User.objects.get(username=self.kwargs['username']))

        return profile



def self_profile_redirect(request):
    return HttpResponseRedirect(
        reverse('profile-detail',
                kwargs={'username': request.user}))
