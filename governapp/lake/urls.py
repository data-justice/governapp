from django.urls import path
from lake import views


urlpatterns = [
    path('',
         views.IslandListView.as_view(),
         name='island-list'),

    path('island/<int:pk>/',
         views.IslandDetailView.as_view(),
         name='island-detail'),
    path('island/add/',
         views.IslandCreateView.as_view(),
         name='island-add'),
    path('island/<int:pk>/update/',
         views.IslandUpdateView.as_view(),
         name='island-update'),
    path('island/<int:pk>/delete/',
         views.IslandDeleteView.as_view(),
         name='island-delete'),
    path('island/<int:pk>/comment/',
         views.IslandCommentView.as_view(),
         name='island-comment'),

    
    path('member-autocomplete/',
         views.MemberAutocomplete.as_view(),
         name='member-autocomplete'),

    path('island/<int:island>/membership/add/',
         views.MembershipCreateView.as_view(),
         name='membership-add'),

    path('island/<int:island>/membership/<int:pk>/delete/',
         views.MembershipDeleteView.as_view(),
         name='membership-delete'),

    path('island/<int:island>/membership/<int:pk>/steward/',
         views.MembershipStewardStatusView.as_view(),
         name='membership-steward'),
    
    path('island/<int:island>/membership/',
         views.MembershipListView.as_view(),
         name='membership'),
    

    
    path('island/<int:island>/issue/<int:pk>/',
         views.IssueDetail.as_view(),
         name='issue-detail'),
    path('island/<int:island>/issue/add/',
         views.IssueCreateView.as_view(),
         name='issue-add'),
    path('island/<int:island>/issue/<int:pk>/update/',
         views.IssueUpdateView.as_view(),
         name='issue-update'),
    path('island/<int:island>/issue/<int:pk>/delete/',
         views.IssueDeleteView.as_view(),
         name='issue-delete'),
    path('island/<int:island>/issue/<int:pk>/comment/',
         views.IssueCommentView.as_view(),
         name='issue-comment'),

    path('island/<int:island>/issue/<int:issue>/standings/',
         views.StandingListView.as_view(),
         name='standing-list'),

    path('island/<int:island>/issue/<int:issue>/standings/<username>/<delta>',
         views.StandingDecIncView.as_view(),
         name='standing-dec-inc'),
    
    path('island/<int:island>/issue/<int:issue>/alternative/<int:pk>/',
         views.AlternativeDetail.as_view(),
         name='alternative-detail'),
    path('island/<int:island>/issue/<int:issue>/alternative/add/',
         views.AlternativeCreateView.as_view(),
         name='alternative-add'),
    path('island/<int:island>/issue/<int:issue>/alternative/<int:pk>/update/',
         views.AlternativeUpdateView.as_view(),
         name='alternative-update'),
    path('island/<int:island>/issue/<int:issue>/alternative/<int:pk>/delete/',
         views.AlternativeDeleteView.as_view(),
         name='alternative-delete'),
    path('island/<int:island>/issue/<int:issue>/alternative/<int:pk>/comment/',
         views.AlternativeCommentView.as_view(),
         name='alternative-comment'),
 
    
    # path('<int:island_id>/issues/<int:issue_id>/distribute-tokens/', views.DistributeTokens.as_view(), name='distribute_tokens'),
    path('vote/<int:alt_id>/',
         views.Vote.as_view(),
         name='vote'),

]
