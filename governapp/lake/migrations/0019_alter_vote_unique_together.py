# Generated by Django 3.2.4 on 2021-07-01 04:45

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('lake', '0018_auto_20210701_0233'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='vote',
            unique_together={('alternative', 'owner')},
        ),
    ]
