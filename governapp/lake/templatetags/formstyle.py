from django import template

register = template.Library()

@register.filter(name='formstyle')
def formstyle(value, arg):
    """
    formstyle shall be a css class for all inputs
    """
    return value.as_widget(attrs={'class': arg})
