from django.contrib import admin
from lake import models

admin.site.site_header = 'Lake Democracy Admin'

class MembershipInline(admin.TabularInline):
    model = models.Membership
    fk_name = 'island'
    extra = 0
    fields = ['member', 'status', ]


class IssueInline(admin.TabularInline):
    model = models.Issue
    fk_name = 'island'
    extra = 0
    show_change_link = True
    fields = ['title', 'description', 'government', 'custodian', ]



@admin.register(models.Island)
class IslandAdmin(admin.ModelAdmin):
    model = models.Island

    list_display = ['name', 'custodian', 'default_government' ]

    search_fields = [
                     'custodian__user__first_name',
                     'custodian__user__last_name',
                     'custodian__user__email',
                     'name']
    list_filter = ['default_government', ]

    inlines = [MembershipInline, IssueInline ]


@admin.register(models.Membership)
class MembershipAdmin(admin.ModelAdmin):
    model = models.Membership
    list_display = ['island', 'member', 'status' ]

    search_fields = [
                     'member__user__first_name',
                     'member__user__last_name',
                     'member__user__email',
                     'island__name']

    list_filter = ['status', ]


class VoteInline(admin.TabularInline):
    model = models.Vote
    fk_name = 'alternative'
    extra = 0
    show_change_link = False
    fields = ['custodian', 'scale', 'anonymous', ]



# @admin.register(models.Alternative)
class AlternativeAdmin(admin.ModelAdmin):
    model = models.Alternative
    list_display = ['title', 'issue']

    inlines = [VoteInline, ]


@admin.register(models.Vote)
class VoteAdmin(admin.ModelAdmin):
    model = models.Vote
    list_display = ['alternative', 'issue', 'voter', 'anonymous', 'magnitude']


class AlternativeInline(admin.TabularInline):
    model = models.Alternative
    fk_name = 'issue'
    extra = 0
    show_change_link = True
    fields = ['title', 'description', ]


class StandingInline(admin.TabularInline):
    model = models.Standing
    fk_name = 'issue'
    extra = 0


@admin.register(models.Issue)
class IssueAdmin(admin.ModelAdmin):
    model = models.Issue
    list_display = ['title', 'island']

    inlines = [AlternativeInline, StandingInline]

    def save_model(self, request, obj, form, change):
        obj.custodian = request.user
        super().save_model(request, obj, form, change)

        if not change:
            # new issue

            island = obj.island
            member_num = island.membership_set.count()
            if member_num > 0:
                tokens_per_member = float(obj.max_tokens) / member_num
                for membership in island.membership_set.all():
                    standing = models.Standing(member=membership.member,
                                               issue=obj,
                                               tokens=tokens_per_member)
                    standing.save()



@admin.register(models.Standing)
class StandingAdmin(admin.ModelAdmin):
    model = models.Standing

    list_display = ['issue', 'member']



@admin.register(models.Profile)
class StandingAdmin(admin.ModelAdmin):
    model = models.Profile

    list_display = ['user', ]
