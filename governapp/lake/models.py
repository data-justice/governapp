from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from django.utils.translation import gettext_lazy as _


class Island(models.Model):
    name = models.CharField(max_length=255, verbose_name=_("name"), help_text=_("name"))
    description = models.TextField(verbose_name=_("description"), help_text=_("description"))
    default_government = models.CharField(max_length=15,
                                          verbose_name=_("default government"),
                                          help_text=_("default government"),
                                          choices=(('centralized', _('centralized')),
                                                   ('aristocratic', _('aristocratic')),
                                                   ('democratic', _('democratic'))))
    custodian = models.ForeignKey(User,
                                  verbose_name=_("custodian"),
                                  help_text=_("custodian"),
                                  on_delete=models.CASCADE)

    avatar = models.ImageField("project avatar",
                               upload_to="island_avatar/%Y/%m/%d",
                               blank=True, null=True)


    def get_absolute_url(self):
        return reverse('island-detail', kwargs={'pk': self.pk})

    def __str__(self):
        return "%s" % self.name

    def members(self):
        return [m.member for m in self.membership_set.all()]


    def stewards(self):
        return [m.member for m in self.membership_set.filter(status='steward')]


class Membership(models.Model):
    member = models.ForeignKey(User,
                               verbose_name=_("member"),
                               help_text=_("member"),
                               on_delete=models.CASCADE)

    island = models.ForeignKey(Island,
                               verbose_name=_("island"),
                               help_text=_("island"),
                               on_delete=models.CASCADE)

    status = models.CharField(max_length=15,
                              verbose_name=_("status"),
                              help_text=_("status"),
                              default='member',
                              choices=(
                                  ('member', _('member')),
                                  ('steward', _('steward')),
                              ))

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['island', 'member'],
                                    name="island-member"),
        ]


    def get_absolute_url(self):
        return reverse('island-detail', kwargs={'pk': self.island.pk})

    def __str__(self):
        return "%s %s" % (self.member,
                          self.island)



class Issue(models.Model):
    island = models.ForeignKey(Island,
                               verbose_name=_("island"),
                               help_text=_("island"),
                               on_delete=models.CASCADE)
    title = models.CharField(max_length=255, verbose_name=_("title"), help_text=_("title"))
    description = models.TextField(verbose_name=_("description"), help_text=_("description"))
    custodian = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name=_("custodian"), help_text=_("custodian"))
    government = models.CharField(max_length=15,
                                  verbose_name=_("government"),
                                  help_text=_("government"),
                                  choices=(('centralized', _('centralized')),
                                           ('aristocratic', _('aristocratic')),
                                           ('democratic', _('democratic'))))

    max_tokens = models.SmallIntegerField(default=10, verbose_name=_("max_tokens"), help_text=_("max_tokens"))

    created = models.DateTimeField(auto_now_add=True, verbose_name=_("created"), help_text=_("created"))

    def outcome(self):
        decision = [(a, a.vote_set.count())
                    for a in self.alternative_set.all()]

        return decision

    def get_absolute_url(self):
        return reverse('issue-detail', kwargs={'island': self.island.id,
                                               'pk': self.pk})

    def __str__(self):
        return self.title



class Alternative(models.Model):
    issue = models.ForeignKey(Issue, on_delete=models.CASCADE, verbose_name=_("issue"), help_text=_("issue"))

    title = models.CharField(max_length=255, verbose_name=_("title"), help_text=_("title"))
    description = models.TextField(verbose_name=_("description"), help_text=_("description"))

    created = models.DateTimeField(auto_now_add=True, verbose_name=_("created"), help_text=_("created"))

    author = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name=_("author"), help_text=_("author"))

    def votes(self):
        m = 0

        if self.issue.government == 'aristocratic':
            for v in self.vote_set.all():
                m += v.magnitude * Standing.objects.get(issue=self.issue,
                                                        member=v.voter).tokens
        else:
            for v in self.vote_set.all():
                m += v.magnitude

        return m

    def __str__(self):
        return "%s" % self.title


class Vote(models.Model):
    alternative = models.ForeignKey(Alternative, on_delete=models.CASCADE, verbose_name=_("alternative"), help_text=_("alternative"))

    voter = models.ForeignKey(User,
                              on_delete=models.CASCADE,
                              verbose_name=_("voter"),
                              help_text=_("person who emits vote"))

    magnitude = models.SmallIntegerField(default=0,
                                         verbose_name=_("magnitude"),
                                         help_text=_("this much for or against alternative"))

    anonymous = models.BooleanField(default=False,
                                    verbose_name=_("anonymous"),
                                    help_text=_("cast anonymous vote"))


    date = models.DateTimeField(auto_now_add=True,
                                verbose_name=_("date"),
                                help_text=_("when vote was cast"))

    class Meta:
        unique_together = ('alternative', 'voter')

    def issue(self):
        return self.alternative.issue

    def __str__(self):
        return "%s %s" % (self.alternative, self.custodian)



class Standing(models.Model):
    member = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name=_("member"), help_text=_("member"))

    issue = models.ForeignKey(Issue, on_delete=models.CASCADE, verbose_name=_("issue"), help_text=_("issue"))

    tokens = models.SmallIntegerField(default=0, verbose_name=_("tokens"), help_text=_("tokens"))

    def __str__(self):
        return "%s [%s]" % (self.member, self.tokens)



class Profile(models.Model):
    user = models.OneToOneField(User,
                                on_delete=models.CASCADE)
    website = models.URLField(default='', blank=True)
    bio = models.TextField(default='', blank=True)
