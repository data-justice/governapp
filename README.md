# Governapp

Esta es una implementación web del sistema de islas de confianza.


# Casos de uso

- Crear isla

- Elegir modelo de gobernanza para isla

- Invitar personas a isla

- Aceptar invitación

- Asignar peso de voto a isleños

- Someter decisión a voto. Se plantea un asunto, se plantean alternativas. Se acopia el voto de la isla.

- Votar por alternativa.



# Lo que la isla resguarda

La isla hace la función secretarial de registrar quién participa, qué
decisiones colectivas se toman, y con qué votos.

También puede hacer valer la decisión colectiva a través de candados
que resguarden una colección de archivos. Tal vez la mejor forma de
hacerlo sea a través del sistema distribuido [Syncthing](https://syncthing.net/).


# Instalar

```
cd governapp

virtualenv -p python3 venv3
source venv3/bin/activate

pip install -r requirements.txt

./manage.py tailwind start
./manage.py runserver

```
