# Consideraciones para el desarrollo de plantillas HTML

Este documento describe brevemente la app, los objetos que la
componen, el mecanismo de herencia del sistema de plantillas y nuestra
elección de *framework* de hojas de estilo.

## 1. Acerca de la aplicación

Nos referimos a la aplicación como "governapp" o como "Lake Democracy".

Se trata de una aplicación para toma de decisiones colectiva. 

Toma como inspiración las islas del lago Titikaka. En ese lago cada
isla es habitada por una comunidad, y tiene un modelo de gobierno
independiente. Son islas artificiales, flotantes, de modo que para
tener autonomía en las decisiones a veces algunos habitantes se
separan y forman su propia isla.

Nuestra app se llama "Lake Democracy", pues en el lago es donde flotan
las islas. Cada isla representa un grupo de personas, se reunen en la
app para tomar decisiones en grupo.

### 1.1 Modelos de Gobierno

Cada isla elige el modelo de gobierno a través del cuál se toman las
decisiones. Puede ser:

 - Democrático. Cada miembro de la isla emite su voto acerca de las
   alternativas por cada issue. Todos los votos cuentan igual, y se
   puede votar de manera anónima.

 - Aristocrático. Al crear issues se crea un número finito de tokens
   que se distribuyen equitativamente entre los miembros de la
   isla. Los miembros pueden dar sus tokens a otros miembros, para
   representar la ponderación (standing) de esos miembros en la
   isla. Los votos de miembros con más *standing* cuentan más, pues la
   puntuación de cada alternativa es la sumatoria de votos
   multiplicados por *standings*.

 - Centralizado. La app ayuda organizando los issues y
   alternativas. Permite llevar una memoria de la
   conversación. También registra votaciones. Pero una sóla persona
   toma las decisiones para todo el grupo.
   
### 1.2 Payload

Nuestra visión es que la toma de decisiones ocurre en torno a un bien
común, un *asset* digital. Esa parte de la app aún está siendo
diseñada, pero para tenerlo en consideración lo llamamos **Payload** y
incluimos como un campo de texto Markdown que se desplegará como parte
de la isla. Puede ser una liga de [SyncThing](https://syncthing.net/),
un [Magnet Link](https://es.wikipedia.org/wiki/Magnet), las
credenciales para una carpeta compartida en la nube o algo parecido.

## 2 Objetos en la app

En el **Lago** flotan **Islas** que son habitadas por sus
**Miembros**. En las Islas se decide acerca de **Issues** eligiendo
**Alternativas** a través de la emisión de **Votos**. Los Miembros
registran sus puntos de vista acerca de Issues y Alternativas
registrando sus **Comentarios**, siempre asociados a Issues.

### 2.1 Modelo de datos

En la siguiente gráfica se muestran los objetos que registra la base
de dato junto con sus campos y relaciones.

![data model](data_model.png)


### 2.2 Jerarquía de objetos

En el siguiente diagrama las flechas representan pertenencia, e.g. una
**Isla** pertenece al **Lago** y un **Miembro** pertenece a una
**Isla**.

![jerarquia de objetos](object_hierarchy.png)


### 2.3. Estructura de plantillas

Esta maqueta muestra cómo los objetos se contienen entre sí. Es sólo
esquemática de qué objetos quizá convenga mostrar juntos, no pretende
sugerir layouts ni colores ;-)

![estructura de objetos](object_embedding.png)



### 2.4 Sistema de plantillas

El marco de desarrollo permite la creación de plantillas con
herencia. Es decir, se puede crear una plantilla base que defina
bloques donde se espera que habrá variación. Luego se desarrollan las
variantes de esos bloques que, a pesar de sus diferencias, mantienen
los rasgos que heredan de la plantilla base.

![ejemplo de herencia](template_example.png)


### 2.5 Hojas de estillo Bootstrap 4.4

Para ahorrar esfuerzos en el desarrollo de un sistema de plantillas
"responsivo" usamos Bootstrap.

<https://getbootstrap.com/docs/4.4/getting-started/introduction/>
